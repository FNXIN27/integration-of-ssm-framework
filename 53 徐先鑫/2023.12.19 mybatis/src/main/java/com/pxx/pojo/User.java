package com.pxx.pojo;
// 创建一个与表名同名的类
public class User {
    // 类的属性与列表的列对应
 private Integer id;
 private String uname;
 private Integer age;

    public User(Integer id, String uname, Integer age) {
        this.id = id;
        this.uname = uname;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "com.pxx.User{" +
                "id=" + id +
                ", uname='" + uname + '\'' +
                ", age=" + age +
                '}';
    }
    public User(){

    }
}
