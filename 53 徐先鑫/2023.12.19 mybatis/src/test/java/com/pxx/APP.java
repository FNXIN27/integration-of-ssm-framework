package com.pxx;

import com.pxx.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class APP {
    @Test
    public  void  testSelect() throws IOException {
        //1、加载mybatis核心配置文件，创建SqlSessionFactory 的对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 2、创建SqlSession对象来执行SQL
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //3、执行sql
        List<User> userlist = sqlSession.selectList("test.selectAll");
        System.out.println(userlist);
        // 用完关闭sqlSeccion
        sqlSession.close();
    }

    @Test
    public void testSelectOne() throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        User user = sqlSession.selectOne("test.selctById", 3);
        System.out.println(user);
        sqlSession.close();


    }
}